//
// Created by nada on 10.10.2017.
//
#include "filesystem.h"

/**
 * Gibt die Dateinamen aller im Ordner befindlichen Dateien an ohne Pfad.
 * @param folder Zu durchsuchender Ordner.
 * @param extension kann sein z.b. *.png.
 * @return Liste der Dateinamen.
 */
std::vector<std::string> Filesystem::get_files(const std::string& folder, const std::string& extension) {
    std::vector<std::string> names;
    std::string search_path = folder + "/" + extension;
    WIN32_FIND_DATA fd;
    HANDLE hFind = FindFirstFile(search_path.c_str(), &fd);
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) {
                names.push_back((std::basic_string<char, std::char_traits<char>, std::allocator<char>> &&) fd.cFileName);
            }
        } while (::FindNextFile(hFind, &fd));
        FindClose(hFind);
    }
    return names;
}

std::vector<std::string> Filesystem::get_subfolders(const std::string& folder) {
    std::vector<std::string> names;
    WIN32_FIND_DATA fd;
    HANDLE hFind = FindFirstFileEx((folder + "/*").c_str(), FindExInfoStandard, &fd, FindExSearchLimitToDirectories, nullptr, 0);
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                std::string subfolder(fd.cFileName);
                if (subfolder != "." && subfolder != ".."){
                    names.push_back(subfolder);
                }
            }
        } while (FindNextFile(hFind, &fd));
        FindClose(hFind);
    }
    return names;
}

void Filesystem::clean_folder(const std::string& folder) {
    DIR* dir = opendir(folder.c_str());
    struct dirent* datei_iter;
    char filepath[0xFF];

    while ((datei_iter = readdir(dir)) != nullptr) {
        sprintf(filepath, "%s/%s", folder.c_str(), datei_iter->d_name);
        remove(filepath);
    }
    closedir(dir);
}

bool Filesystem::create_directory(const std::string& folder) {
    if (CreateDirectory(folder.c_str(), nullptr)) {
        std::cout << "Ordner " << folder << " erstellt." << std::endl;
        return true;
    }
    //std::cerr << "Ordner " << folder << " konnte nicht erstellt werden" << std::endl;
    return false;
}

bool Filesystem::delete_directory(const std::string& folder) {
    if (RemoveDirectory(folder.c_str())) {
        std::cout << "Ordner " << folder << " geloescht." << std::endl;
        return true;
    }
    return false;
}

void Filesystem::copy_file(const std::string& from, const std::string& to) {
    std::ifstream src(from, std::ios::binary);
    std::ofstream dst(to,   std::ios::binary);
    dst << src.rdbuf();
    dst.close();
    src.close();
}
