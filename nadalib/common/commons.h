//
// Created by krizchri on 07.09.2017.
//
#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <random>
#include <algorithm>

/**
 * Enthält einige Allzweck-std::string-Methoden.
 * Werden provisorisch eingesetzt, um auf weitere zusätzliche Bibliotheken (wie z.B. Boost) verzichten zu können.
 */
class Commons {

public:

    /**
     * Liefert einen std::vector aus std::string, die aus einem Text durch einen Separator geteilt wurden.
     * @param text Aufzuteilender Text
     * @param token char, bei dem aufgeteilt wird
     * @return Textteile
     */
    static std::vector<std::string> tokenize(const std::string& text, char token);

    /**
     * Ersetzt in einem std::string die erste gefundene char-Folge durch eine weitere angegebene char-Folge.
     * @param string Zu behandelder std:string
     * @param alt Substring, der ersetzt wird
     * @param neu String, durch den ersetzt wird
     */
    static bool replace(std::string& string, const std::string& alt, const std::string& neu);

    /**
     * Ersetzt in einem std::string alle char-Folgen durch eine weitere angegebene char-Folge.
     * @param string Zu behandelder std:string
     * @param alt Substring, der ersetzt wird
     * @param neu String, durch den ersetzt wird
     */
    static bool replace_all(std::string& string, const std::string& alt, const std::string& neu);

    /**
     * Prüft ob ein String in eine Gleitkommazahl konvertiert werden kann.
     * Möglicherweise existieren seltene Fälle, in denen diese Methode nicht
     * mit 100%iger Zuverlässigkeit operiert.
     * @param float_string Zu überprüfender std::string
     * @return true, wenn möglich; false, wenn nicht
     */
    static bool is_float(const std::string& float_string);

    /**
     * Liefert einen Teilstring zurück. Der Teilstring beginnt einschließlich mit dem ``char start`` und
     * endet ausschließlich mit dem ``char ende``.
     * @param such_string Vollständiger string, von dem ein Teil benötigt wird.
     * @param start char, mit dem der Teilstring beginnen soll.
     * @param ende char, vor dem der Teilstring unmittelbar endet.
     * @return Ein Teilstring von dem vollständigen gegebenen String.
     */
    static const std::string substring(const std::string& such_string, char start, char ende);

    /**
     * Konvertiert einen std::string vollständig in Kleinbuchstaben.
     * Unicode etc. wird nicht berücksichtigt.
     * @param mixed_case_string String gemischt aus Groß-/Kleinschreibung.
     * @return lower case String
     */
    static const std::string to_lower(const std::string& mixed_case_string);


private:

    /**
     * Klasse enthält ausschließlich statische Methoden. Konstruktor ist daher `private`.
     */
    Commons() = default;

};