//
// Created by nada on 12.10.2017.
//
#include "button_choice.h"

ChoiceButton::ChoiceButton(int x, int y, uint16_t width, uint16_t height, std::string text,
                           std::vector<std::string> values)
        :Button(x, y, width, height, text),
         index(0),
         values(values) {
    if (values.empty()) {
        std::cerr << "ChoiceButton created with empty values" << std::endl;
    }
    render();
}

void ChoiceButton::render() const {
    if (mouse_over()) SDL_SetRenderDrawColor( Gfx::RENDERER, Gfx::RGB_BG2[0], Gfx::RGB_BG2[1], Gfx::RGB_BG2[2], 0xFF);
    else SDL_SetRenderDrawColor( Gfx::RENDERER, Gfx::RGB_BG[0], Gfx::RGB_BG[1], Gfx::RGB_BG[2], 0xFF);
    SDL_RenderFillRect( Gfx::RENDERER, &rect );
    SDL_SetRenderDrawColor( Gfx::RENDERER, 0x00, 0x00, 0x00, 0xFF );
    Gfx::blit_text(text + values[index], rect.x + 12, rect.y + rect.h / 2 - 8);
}

void ChoiceButton::update(const SDL_Event* event) {
    if (event->wheel.y == 1 || event->wheel.y == -1) {
        int mx, my;
        SDL_GetMouseState(&mx, &my);
        if (mx < this->rect.x) return;
        if (mx > this->rect.x+this->rect.w) return;
        if (my < this->rect.y) return;
        if (my > this->rect.y+this->rect.h) return;
        if (event->wheel.y == 1 || index > 0) index += event->wheel.y;
        if (index > values.size()-1) index = values.size() -1;
        render();
    }
}

void ChoiceButton::removeValue(std::string value) {
    for (int i=0; i<values.size(); ++i) if (values[i] == value) {
            values.erase(values.begin()+i);
            if (i == index) {
                index = 0;
                set_text(values[index]);
            }
            break;
        }
}
