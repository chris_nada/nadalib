#pragma once

#include <iostream>
#include "Button.h"

class ChoiceButton : public Button {

public:
    ///ctor
    ChoiceButton(int x, int y, uint16_t width, uint16_t height, std::string text, std::vector<std::string> values);

    void render() const override;

    void update(const SDL_Event* event);

    std::string getValue() const { return values[index]; }
    const std::vector<std::string>& getValues() const { return values; }
    void addValue(std::string value) { values.push_back(value); }
    void removeValue(std::string value);

private:
    uint8_t index;
    std::vector<std::string> values;

};