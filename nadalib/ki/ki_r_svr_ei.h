//
// Created by krizchri on 11.09.2017.
//
#pragma once

#include <iostream>
#include <vector>
#include <dlib/svm.h>
#include "ki.h"


/**
 * Maschinenlerntyp:
 * Regressionsanalyse: Support-vector-regression (epsilon-insensitive)
 * - linear unabhängig
 *
 * @tparam sample_type Datentyp der Werte aus der Datenmatrix. In aller Regel ``double``.
 * @tparam spalten Anzahl der Spalten in der Datenmatrix.
 * @tparam zeilen Anzahl der Zeilen in der Datenmatrix.
 */
template<typename sample_type, const long spalten, const long zeilen>
class KI_R_SVR_EI : private KI {

public:

    /**
     * ctor
     */
    explicit KI_R_SVR_EI(const double gamma = 10, const double epsilon = 0.001) {
        set_gamma(gamma);
        set_epsilon_insensitivity(epsilon);
    };

    /**
     * Trainingsdaten hinzufügen. sample = Ordinate, target = abhängige Variable.
     */
    void add_learn_data(const dlib::matrix<sample_type, spalten, zeilen> sample, const sample_type target) {
        samples.push_back(sample);
        targets.push_back(target);
    }

    /**
     * Training mit aktuellem Korpus durchführen
     */
    void train() override {
        std::cout << "KI_R_SVR_EI::train()" << std::endl;
        //KI Traininren. decision_function ist die resultierende, berechneteFunktion
        decision_function = trainer.train(samples, targets);
        std::cout << samples.size() << " Trainingsdaten trainiert." << std::endl;
    }

    /**
     * Vorhersage treffen zu gegebenem Wert.
     */
    const sample_type predict(const dlib::matrix<sample_type, spalten, zeilen> sample) const { return decision_function(sample); }

    /**
     * Legt Gamma fest.
     */
    void set_gamma(const double gamma) { trainer.set_c(gamma); }

    /**
     * Legt Epsilon fest.
     */
    void set_epsilon_insensitivity(const double epsilon) { trainer.set_epsilon_insensitivity(epsilon); }


private:
    std::vector<dlib::matrix<sample_type, spalten, zeilen>> samples; //Trainingskorpus
    std::vector<sample_type>                                targets; //Zieldaten
    dlib::decision_function
            <dlib::radial_basis_kernel
                    <dlib::matrix<sample_type, spalten, zeilen>>> decision_function; //Entscheidungsfunktion
    dlib::svr_trainer
            <dlib::radial_basis_kernel
                    <dlib::matrix<sample_type, spalten, zeilen>>> trainer;           //Trainertyp


};

