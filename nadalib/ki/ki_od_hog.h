//
// Created by nada on 12.11.2017.
//
#pragma once

#include <dlib/svm_threaded.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_processing.h>
#include <dlib/data_io.h>
#include <iostream>
#include <fstream>
#include "ki.h"

/**
 * http://dlib.net/fhog_object_detector_ex.cpp.html
 */
class KI_OD_HOG : private KI {

public:

    KI_OD_HOG(const std::string& training_xml,
              const std::string& testing_xml,
              bool  symmetric = false)
            : training_xml(training_xml),
              testing_xml(testing_xml),
              symmetric(symmetric) {
    }

    void train() override {
        //Trainingsdaten generieren
        dlib::array<dlib::array2d<unsigned char>> images_train,     images_train_m,     images_test;
        std::vector<std::vector<dlib::rectangle>> face_boxes_train, face_boxes_train_m, face_boxes_test;
        load_image_dataset(images_train,   face_boxes_train,   training_xml);
        load_image_dataset(images_test,    face_boxes_test,    testing_xml);
        if (symmetric) add_image_left_right_flips(images_train,  face_boxes_train);
        else {
            load_image_dataset(images_train_m, face_boxes_train_m, training_xml);
            dlib::flip_image_dataset_left_right(images_train_m, face_boxes_train_m);
        }

        bool upsample;
        std::cout << "upsample?             = " << std::flush;
        std::cin >> upsample;
        if (upsample) {
            dlib::upsample_image_dataset<dlib::pyramid_down<2> >(images_train, face_boxes_train);
            dlib::upsample_image_dataset<dlib::pyramid_down<2> >(images_train_m, face_boxes_train_m);
        }

        std::cout << "n training images     = " << images_train.size()   << std::endl;
        std::cout << "n training images (m) = " << images_train_m.size() << std::endl;
        std::cout << "n testing images      = " << images_test.size()  << std::endl;

        //c und epsilon in der Konsole abfragen
        double c, epsilon, d_win_size;
        std::cout << "c (Standard=10)    = " << std::flush;
        std::cin >> c;
        std::cout << "e (Standard=0.03) = " << std::flush;
        std::cin >> epsilon;
        std::cout << "Suchfenstergroesse (Standard=80) = " << std::flush;
        std::cin >> d_win_size;

        //Trainer Konfig.
        image_scanner_type scanner;
        scanner.set_detection_window_size(d_win_size, d_win_size);
        dlib::structural_object_detection_trainer<image_scanner_type> trainer(scanner);
        trainer.set_num_threads(4);
        trainer.set_c(c);
        trainer.set_epsilon(epsilon);
        trainer.be_verbose();

        //Trainingsdaten mit zu start verzerrten Seitenverhältnissen rausschmeißen
        std::vector<std::vector<dlib::rectangle> > removed;
        removed = dlib::remove_unobtainable_rectangles(trainer, images_train, face_boxes_train);

        //Training
        {
            std::ifstream in("detector.svm");
            if (!in.good()) {
                std::cout << "Training startet..." << std::endl;
                detector = trainer.train(images_train, face_boxes_train);
                dlib::serialize("detector.svm") << detector;
                std::cout << "Training beendet." << std::endl;
            } else dlib::deserialize("detector.svm") >> detector;
        }

        //Training gespiegelt
        if (!symmetric) {
            std::ifstream in("detector_m.svm");
            if (!in.good()) {
                std::cout << "Training (m) startet..." << std::endl;
                detector_m = trainer.train(images_train_m, face_boxes_train_m);
                dlib::serialize("detector_m.svm") << detector_m;
                std::cout << "Training beendet (m)." << std::endl;
            } else dlib::deserialize("detector_m.svm") >> detector_m;
        }

        //Auf Testdaten anwenden
        dlib::image_window hogwin1(draw_fhog(detector),     "Learned fHOG detector");
        dlib::image_window hogwin2(draw_fhog(detector_m),   "Learned fHOG detector m");
        dlib::image_window win;
        std::cin.get();
        for (unsigned long i = 0; i < images_test.size(); ++i) {
            std::cout << "Weiter..." << std::flush;
            std::cin.get();
            win.clear_overlay();
            win.set_image(images_test[i]);
            std::vector<dlib::rectangle> dets = detector(images_test[i]);
            win.add_overlay(dets, dlib::rgb_pixel(255,0,0));
            if (!symmetric) {
                std::vector<dlib::rectangle> dets_m = detector_m(images_test[i]);
                win.add_overlay(dets_m, dlib::rgb_pixel(0,255,0));
            }
        }
        std::cin.get();
    }

    void load_detectors(const std::string& detector_a, const std::string& detector_b) {
        dlib::deserialize(detector_a) >> detector;
        dlib::deserialize(detector_b) >> detector_m;
    }


private:

    typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<6>> image_scanner_type;
    dlib::object_detector<image_scanner_type> detector;
    dlib::object_detector<image_scanner_type> detector_m;
    bool        symmetric;
    std::string training_xml;
    std::string testing_xml;

};