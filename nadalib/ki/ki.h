//
// Created by krizchri on 18.09.2017.
//

#pragma once

/**
 * Abstrakte Klasse als Basis zur Implementierung verschiedener KI-Algorithmen.
 */
class KI {

public:

    virtual void train() = 0;

};