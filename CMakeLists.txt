cmake_minimum_required(VERSION 3.9)
project(nadalib)

include(GNUInstallDirs)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -s -m64 -mtune=generic -DDLIB_JPEG_SUPPORT -DDLIB_PNG_SUPPORT") # -DDLIB_USE_CUDA

add_library(nadalib SHARED
        nadalib/nadalib.h
        nadalib/common/dice.h
        nadalib/common/dice.cpp
        nadalib/common/commons.h
        nadalib/common/commons.cpp
        nadalib/common/filesystem.h
        nadalib/common/filesystem.cpp
        nadalib/gfx/gfx.h
        nadalib/gfx/gfx.cpp
        nadalib/gfx/asset.h
        nadalib/gfx/asset.cpp
        nadalib/gfx/utils.h
        nadalib/gfx/utils.cpp
        nadalib/gui/node.h
        nadalib/gui/button.h
        nadalib/gui/button.cpp
        nadalib/gui/button_choice.h
        nadalib/gui/button_choice.cpp
        nadalib/gui/button_increment.h
        nadalib/gui/button_increment.cpp
        nadalib/gui/input_text.h
        nadalib/gui/input_text.cpp
        nadalib/gui/progressbar.h
        nadalib/gui/progressbar.cpp
        nadalib/gui/textarea.h
        nadalib/gui/textarea.cpp
        nadalib/gui/label.h
        nadalib/gui/label.cpp
        nadalib/gui/dialog.h
        nadalib/ki/ki.h
        nadalib/ki/ki_od_hog.h
        nadalib/ki/ki_od_mmod.h
        nadalib/ki/ki_c_images.h
        nadalib/ki/ki_c_kkmc.h
        nadalib/ki/ki_c_mlp.h
        nadalib/ki/ki_r_krr.h
        nadalib/ki/ki_r_svr_ei.h
        nadalib/ki/ki_r_krls.h
        nadalib/ki/ki_c_krr.h
        )

set_target_properties(nadalib PROPERTIES
        OUTPUT_NAME nadalib
        VERSION 0.1.0)

configure_file(nadalib.pc.in nadalib.pc.in @ONLY)
target_include_directories(nadalib PRIVATE .)
install(TARGETS nadalib
        RUNTIME DESTINATION       ${CMAKE_INSTALL_BINDIR}
        LIBRARY DESTINATION       ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION       ${CMAKE_INSTALL_LIBDIR}
        PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
        )
install(FILES ${CMAKE_BINARY_DIR}/nadalib.pc
        DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/pkgconfig
        )

include(../dlib-19.7/dlib/cmake)
include_directories(../nadalib)

find_package(PkgConfig     REQUIRED)
pkg_check_modules(SDL2     REQUIRED sdl2)
pkg_check_modules(SDL2_GFX REQUIRED SDL2_gfx)
pkg_check_modules(SDL2_IMG REQUIRED SDL2_image)
pkg_check_modules(SDL2_TTF REQUIRED SDL2_ttf)
pkg_check_modules(SDL2_MIX REQUIRED SDL2_mixer)
pkg_check_modules(SDL2_NET REQUIRED SDL2_net)
pkg_check_modules(LIBJPEG  REQUIRED LIBJPEG)
pkg_check_modules(LIBPNG   REQUIRED LIBPNG)

include_directories(
        ${SDL2_INCLUDE_DIRS}
        ${SDL2_GFX_INCLUDE_DIRS}
        ${SDL2_IMG_INCLUDE_DIRS}
        ${SDL2_TTF_INCLUDE_DIRS}
        ${SDL2_NET_INCLUDE_DIRS}
        ${SDL2_MIX_INCLUDE_DIRS}
        ${LIBJPEG_INCLUDE_DIRS}
        ${LIBPNG_INCLUDE_DIRS}
)

link_directories (
        ${SDL2_LIBRARY_DIRS}
        ${SDL2_GFX_INCLUDE_DIRS}
        ${SDL2_IMG_LIBRARY_DIRS}
        ${SDL2_TTF_LIBRARY_DIRS}
        ${SDL2_NET_LIBRARY_DIRS}
        ${SDL2_MIX_LIBRARY_DIRS}
        ${LIBJPEG_LIBRARY_DIRS}
        ${LIBPNG_LIBRARY_DIRS}
)

target_link_libraries (nadalib
        ${SDL2_LIBRARIES}
        ${SDL2_GFX_LIBRARIES}
        ${SDL2_IMG_LIBRARIES}
        ${SDL2_TTF_LIBRARIES}
        ${SDL2_MIX_LIBRARIES}
        ${SDL2_NET_LIBRARIES}
        dlib::dlib
        ${LIBJPEG_LIBRARIES}
        ${LIBPNG_LIBRARIES}
        gdi32 comctl32 user32 winmm ws2_32 imm32 #dlib
        )

